import { HttpModule } from '@angular/http';
import { DataService } from '../../services/data.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BanksComponent } from './banks.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BankCityFilterPipe } from '../../filters/bankCityFilter.pipe';
import { of } from 'rxjs';
import { NO_ERRORS_SCHEMA, Component, Input} from '@angular/core';

describe('BanksComponent', () => {
  let component: BanksComponent;
  let fixture: ComponentFixture<BanksComponent>;
  let mockDataService;
  let bankData;

  beforeEach(async(() => {
    mockDataService = jasmine.createSpyObj(['getData']);
    TestBed.configureTestingModule({
      declarations: [ BanksComponent, BankCityFilterPipe],
      imports: [HttpModule, FormsModule, NgxPaginationModule],
      // When any part of the test code asks for a DataService, provide the mockDataService instead.
      providers: [{provide: DataService, useValue: mockDataService}],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(BanksComponent);

    bankData = [
      {
        Identification: '11801600',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 24 BRIDGE STREET'],
          Country: 'GB',
          PostCode: 'L9 2AU',
          TownName: 'TADCASTER'
        },
        Name: 'TADCASTER'
      },
      {
        Identification: '11863000',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 78-80 WALTON VALE'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      },
      {
        Identification: '11863100',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 40 SIMTON STREET'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      }
    ];
  }));

  it('should create', () => {
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should filter the number of matching city names', () => {
    // Arrange
    component = fixture.componentInstance;
    fixture.componentInstance.appData = bankData;
    fixture.componentInstance.term2bfiltered = 'walton vale';
    // Act
    component.getNumberOfFilteredBankLocations();
    // Assert
    expect(fixture.componentInstance.numberOfItems).toBe(2);
  });

});
