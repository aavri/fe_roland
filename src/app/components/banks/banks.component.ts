import { DataService } from '../../services/data.service';
import { Component, OnInit, Input} from '@angular/core';
import { BankCityFilterPipe } from '../../filters/bankCityFilter.pipe';
import { BankComponent } from '../bank/bank.component';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.css']
})
export class BanksComponent {
  @Input() appData: Array<BankComponent>;
  bankCityFilter = new BankCityFilterPipe();
  term2bfiltered = ''; // Stays in synch with the search term input
  pge = 1;
  numberOfItems;

  constructor(private genericDataService: DataService) { }

  getNumberOfFilteredBankLocations() {
    this.numberOfItems = this.bankCityFilter.transform(this.appData, this.term2bfiltered).length;
  }


  transmitBankDetails(itm): void {
    this.genericDataService.bankDataTransmitted.next(itm);
  }
}
