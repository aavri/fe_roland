import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent {
  @Input() bankDetails: BankComponent;
  @Input() indexx: number;
  @Input() numberOfItems: Array<object>;
}
