import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankComponent } from './bank.component';

describe('BankComponent', () => {
  let fixture: ComponentFixture<BankComponent>;
  let bankData;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankComponent ]
    });

    // Use the TestBed module above and create the component
    fixture = TestBed.createComponent(BankComponent);

      bankData = [
      {
        Identification: '11801600',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 24 BRIDGE STREET'],
          Country: 'GB',
          PostCode: 'L9 2AU',
          TownName: 'TADCASTER'
        },
        Name: 'TADCASTER'
      },
      {
        Identification: '11863000',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 78-80 WALTON VALE'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      },
      {
        Identification: '11863100',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 40 SIMTON STREET'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      }
    ];
  }));

  it('should be created', () => {
    // Use the fixture property to get an instance of the component
    fixture.componentInstance.bankDetails = bankData[0];
    expect(fixture.componentInstance).toBeTruthy();
  });


  it('should render the TownName in the template', () => {
    fixture.componentInstance.bankDetails = bankData[0];
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#town-name').textContent).toContain('TADCASTER');
  });


});
