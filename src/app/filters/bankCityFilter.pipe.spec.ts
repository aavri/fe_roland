import { BankCityFilterPipe } from './bankCityFilter.pipe';

describe('FilterPipe', () => {
  let dataArray;

  beforeEach(() => {
    dataArray = [
      {
        Identification: '11801600',
        PostalAddress: {
        AddressLine: ['HALIFAX BRANCH 24 BRIDGE STREET'],
        Country: 'GB',
        PostCode: 'L9 2AU'
      }, Name: 'TADCASTER'},
      {
        Identification: '11863000',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 78-80 WALTON VALE'],
          Country: 'GB',
          PostCode: 'L9 2BU'
        },
        Name: 'WALTON VALE'
      },
      {
        Identification: '11863100',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 40 SIMTON STREET'],
          Country: 'GB',
          PostCode: 'L9 2BU'
        },
        Name: 'WALTON VALE'
      }
    ];
  });

  it('create an instance', () => {
    const cityFilter = new BankCityFilterPipe();

    const filtered = cityFilter.transform(dataArray, 'walton vale');

    expect(filtered.length).toBeTruthy(2);
  });
});
