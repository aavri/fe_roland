import { Pipe, PipeTransform } from '@angular/core';
import { BankComponent } from '../components/bank/bank.component';

@Pipe({
  name: 'bankfilter'
})
export class BankCityFilterPipe implements PipeTransform {

  transform(value, filterString: string): Array<BankComponent> {
    const filteredResultsArray = [];
    const filteredTerm = new RegExp('^' + filterString, 'i');

    if (!value && !(value instanceof Array)) {
      return value;
    }

    for (const item of value) {
      if (filteredTerm.test(item.Name)) {
        filteredResultsArray.push(item);
      }
    }

    return filteredResultsArray;
  }

}
