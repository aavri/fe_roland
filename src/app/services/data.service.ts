import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {}

  bankDataTransmitted = new Subject();

  public getData(): Observable<any> {
    return this.http.get('https://api.halifax.co.uk/open-banking/v2.2/branches');
  }
}
