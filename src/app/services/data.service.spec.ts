import { TestBed, inject } from '@angular/core/testing';

import { DataService} from './data.service';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { observable, of } from 'rxjs';

// This is an intergration test
describe('DataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        DataService
      ]
    });

    // This is how to get a handle to the http service so that we can control it in our test.
    // The TestBed.get method helps us to access the dependency injection registry, but the inject method will be used instead
    /* let httpTestingController = TestBed.get(HttpTestingController); */

    // Alternative way to using the inject() method, of getting a handle to the DataServiceService
    //     being tested or any other dependable service to be tested alongside, since this is an
    //     integration test, and this is how to get an instance of a service within our testing module.
    /* let dataService = TestBed.get(DataService); */
  } );

  it('should be created', () => {
    const service: DataService = TestBed.get(DataService);
    expect(service).toBeTruthy();
  });

  describe('getData', () => {
    it('should return the proper data',
    inject(
      [HttpTestingController, DataService],
      (httpController: HttpTestingController, dataService: DataService) => {
        // Now we call our main service to be tested
        dataService.getData().subscribe(returnedData => {
          expect(returnedData.data[0].Branch[0].Identification).toEqual('11863000');
          expect(returnedData.meta.LastUpdated).toEqual('2018-09-24T16:06:58.188Z');
        });

        // Set the expectations that test for what kind of requst method was called with the URL
        const req = httpController.expectOne('https://api.halifax.co.uk/open-banking/v2.2/branches', 'Trying to reach the endpoint');
        expect(req.request.method).toEqual('GET'); // Then test if a get request would be made

        // Set the fake data to be returned by the mock
        req.flush({
          data: [{Branch: [{Identification: '11863000', SequenceNumber: '00', Name: 'TADCASTER', Type: 'Physical'}]}],
          meta: {LastUpdated: '2018-09-24T16:06:58.188Z', TotalResults: 617, Agreement: 'Use of the APIs and any'}
        });

    }));


  });

  afterEach(inject([HttpTestingController], (httpController: HttpTestingController) => {
    httpController.verify(); // Verify the http requests after each test, for example that the expectOne() is making the call only once.
  }));

});
