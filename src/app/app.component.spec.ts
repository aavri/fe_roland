import {HttpModule} from '@angular/http';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { Component, Input } from '@angular/core';
import { TestBed, async, fakeAsync, inject, ComponentFixture} from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BanksComponent } from './components/banks/banks.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BankCityFilterPipe } from './filters/bankCityFilter.pipe';
import { DataService } from './services/data.service';
import { NO_ERRORS_SCHEMA} from '@angular/core';


describe('AppComponent', () => {
  let bankData;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, BanksComponent, BankCityFilterPipe],
      imports: [HttpModule, HttpClientTestingModule, FormsModule, NgxPaginationModule],
      schemas: [NO_ERRORS_SCHEMA]
    });

    bankData = [
      {
        Identification: '11801600',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 24 BRIDGE STREET'],
          Country: 'GB',
          PostCode: 'L9 2AU',
          TownName: 'TADCASTER'
        },
        Name: 'TADCASTER'
      },
      {
        Identification: '11863000',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 78-80 WALTON VALE'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      },
      {
        Identification: '11863100',
        PostalAddress: {
          AddressLine: ['HALIFAX BRANCH 40 SIMTON STREET'],
          Country: 'GB',
          PostCode: 'L9 2BU',
          TownName: 'WALTON VALE'
        },
        Name: 'WALTON VALE'
      }
    ];

  }));

  it('should create one list item for each bank', inject(
    [HttpTestingController, DataService],
    (httpController: HttpTestingController, dataService: DataService) => {
      dataService.getData().subscribe(returnedData => {
        expect(returnedData[0].Name).toBe('TADCASTER');
      });

      // Set the expectations that test for what kind of requst method was called with the URL
      const req = httpController.expectOne('https://api.halifax.co.uk/open-banking/v2.2/branches', 'Trying to reach the endpoint');
      expect(req.request.method).toEqual('GET'); // Then test if a get request would be made

      // Set the fake data to be returned by the mock
      req.flush(bankData);
  }));

});
