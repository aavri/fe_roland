import { Component, OnInit, OnDestroy} from '@angular/core';
import { DataService } from './services/data.service';
import { Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  appData: Array<object>;

  objectToTransmitt: object;
  subscription: Subscription;
  private bankDetailsObject: any;


  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    this.subscription = this.dataService.bankDataTransmitted.subscribe(item => {
      this.bankDetailsObject = item;
    });

    this.dataService.getData()
    .subscribe(
      (returnedData: Response) => {
        const resp = returnedData['data'][0];
        this.appData = resp.Brand[0].Branch;
      },

      (error) => {
        console.log('An Error occured ', error);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

