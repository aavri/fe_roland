#  ************** Instructions ******************

1) Extract the files from the zip file and place the directory of your choice

2) Using the command line or terminal, navigate to the directory that contains the "package.json"
    file, and then run the command:
    npm install

3) When all the dependencies have been installed through the npm install command, run the following 
   command to open the project in the default browser:
   ng serve --open 

4) To run the Unit/Integration testes, run the following command in the root directory:
    npm test


